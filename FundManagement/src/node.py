import logging

logging.basicConfig()
LOG = logging.getLogger('FundManagement')
LOG.setLevel(logging.INFO)


class Node:
    def __init__(self, name, level):
        self.name = name
        self.connected_to = dict()
        self.level = level

    def add_neighbour(self, nbr_node, weight):
        self.connected_to[nbr_node] = weight

    def __str__(self):
        ret_st = ""
        for n in self.connected_to:
            ret_st = ret_st + "," + n.name
        return ret_st

    # to get the name of any node, if required
    def get_name(self):
        return self.name

    # to get the weight of any edge, if required
    def get_weight(self, nbr_node):
        return self.connected_to[nbr_node]
