from src.node import Node

import logging

logging.basicConfig()
LOG = logging.getLogger('FundManagement')
LOG.setLevel(logging.INFO)


class Graph:

    def __init__(self):
        self.graph_dict = dict()
        self.num_vertices = 0
        self.holding_value = dict()

    def add_node(self, vtx, level, weight=0, holding_value=0):
        LOG.info("Adding node : {}".format(vtx))
        if level == 3:
            if weight <= 0:
                raise Exception("Weight for leaf node must be greater than Zero")
        if level == 3:
            new_vertex = Node(name=vtx, level=level)
            self.holding_value[vtx] = holding_value
        else:
            new_vertex = Node(name=vtx, level=level)
        self.graph_dict[vtx] = new_vertex
        self.num_vertices += 1
        return new_vertex

    def create_relationship(self, pvtx, cvtx, p_level, c_level, weight=0, holding_value=0):
        if pvtx not in self.graph_dict:  # if parent node not found in existing graph
            LOG.info("node {} not found in graph, so creating..".format(pvtx))
            if p_level == 3:
                if weight <= 0:
                    raise Exception("Invalid weight value: ", weight)
            self.add_node(pvtx, level=p_level, weight=weight, holding_value=holding_value)
        if cvtx not in self.graph_dict:  # if child node not found in existing graph
            LOG.info("node {} not found in graph, so creating..".format(cvtx))
            if c_level == 3:
                if weight <= 0:
                    raise Exception("Invalid weight value: ", weight)
            self.add_node(cvtx, level=c_level, weight=weight, holding_value=holding_value)

        self.graph_dict[pvtx].add_neighbour(self.graph_dict[cvtx], weight)

    def get_connected_vertices(self, n):
        if n in self.graph_dict:
            return str(self.graph_dict[n])

    def get_holding_value_of_a_node(self, node):
        try:
            return self.holding_value[node]
        except KeyError:  # i.e. the node is not a fund-id, but is an investor-id
            raise KeyError

    def retrieve_fund_market_value(self, node, exclude):
        if node:
            if self.graph_dict[node].level == 2:  # if the query has been done via fund-id and NOT investor-id
                fund_value = 0
                connected_values = self.get_connected_vertices(node)
                LOG.info("connected vertices to node {} are: {}".format(node, connected_values))
                connected_values = connected_values.strip(',').split(',')
                for each_node in connected_values:
                    if each_node not in list(exclude):  # the exclude-condition check
                        fund_value += (self.get_holding_value_of_a_node(each_node)) * 100
                return fund_value
            elif self.graph_dict[node].level == 1:
                # we need to find the funds of this investor:
                connected_values = self.get_connected_vertices(node)
                connected_values = connected_values.strip(',').split(',')
                LOG.info("connected vertices of node {} are {}".format(node, connected_values))
                fund_value = 0
                for fund_id in connected_values:
                    fund_value += self.retrieve_fund_market_value(fund_id, exclude=[])
                return fund_value

            else:
                raise Exception("Invalid type of node")
