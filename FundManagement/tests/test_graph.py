from src.graph import Graph
from src.node import Node

import unittest


class TestNode(unittest.TestCase):

    def setUp(self) -> None:
        # we can set some set up values if required
        self.g = Graph()

    def tearDown(self) -> None:
        # destroy any object(s) if you want to
        pass

    def test_add_node(self):
        vertex = self.g.add_node('I1', level=1, holding_value=0)
        self.assertIsInstance(vertex, Node, msg='return value type is not of Node class')
        self.assertIn('name', vertex.__dict__, msg='name attribute not found in object')
        self.assertEqual(vertex.name, 'I1', msg='name of the vertex is not correct')

    def test_raise_exception(self):
        # test case to check if exception is raised
        self.assertRaises(Exception, self.g.add_node, 'H1', 3, -1)

    def test_get_holding_value(self):
        node = self.g.add_node('H1', level=3, holding_value=50, weight=1)
        self.assertEqual(self.g.get_holding_value_of_a_node('H1'), 50, msg='Holding value is not correct')

    # More integration test cases can be added...

