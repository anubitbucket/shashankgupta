from src.node import Node

import unittest


class TestNode(unittest.TestCase):
    def test_get_name_of_node(self):
        node = Node(name='test_fund', level=2)
        self.assertEqual(node.get_name(), 'test_fund')

    def test_add_neighbour(self):
        node = Node(name='test_holding', level=3)
        node.add_neighbour('F1', 4)
        node.add_neighbour('F2', 5)
        self.assertEqual(node.get_weight('F1'), 4)
        self.assertEqual(node.get_weight('F2'), 5)
