from src.graph import Graph


if __name__ == '__main__':
    g = Graph()

    # adding a simple node of level 1
    g.add_node('I1', level=1, holding_value=0)

    # creating relationships
    g.create_relationship('I1', 'F1', p_level=1, c_level=2)  # as level of any node is not 3, so holding value not required
    g.create_relationship('I1', 'F2', p_level=1, c_level=2)  # as level of any node is not 3, so holding value not required
    g.create_relationship('I1', 'F3', p_level=1, c_level=2)  # as level of any node is not 3, so holding value not required
    g.create_relationship('F1', 'H1', p_level=2, c_level=3, weight=2, holding_value=10)
    g.create_relationship('F1', 'H2', p_level=2, c_level=3, weight=8, holding_value=20)
    g.create_relationship('F1', 'H4', p_level=2, c_level=3, weight=5, holding_value=10)
    g.create_relationship('F2', 'H2', p_level=2, c_level=3, weight=5, holding_value=20)
    g.create_relationship('F3', 'H4', p_level=2, c_level=3, weight=4, holding_value=10)

    # calculating fund value for Fund 'F1' without exclusion
    market_value_fund1 = g.retrieve_fund_market_value('F1', exclude=[])

    # calculating fund value for Investor 'I1' without exclusion
    market_value_investor1 = g.retrieve_fund_market_value('I1', exclude=[])

    # calculating fund value for Fund 'F1' and excluding 'H1' holding
    market_value_holding1 = g.retrieve_fund_market_value('F1', exclude=['H1'])

    print("\n******************************************************************")
    print("Market value of Fund-1 is: {0}".format(market_value_fund1))
    print("Market value of Investor-1 is: {0}".format(market_value_investor1))
    print("Market value of Fund-1 with H1 exclusion is: {0}".format(market_value_holding1))
    print("******************************************************************\n")
