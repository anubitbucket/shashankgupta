This is a readme file for explaining the FundManagement package.

__To run the code__, go inside the project directory (FundManagement) and type the following command and press enter:
```python run.py```

The package constains 2 files -

a) __node.py__ which contains the class __Node__ which encapsulates the functions related to creation of a node/vertex

b) __graph.py__ with the class __Graph__ which contains functions related to linking of nodes, traversing, etc.


Understanding the ```if __name__ == __main__``` section of the __run.py__ file -

First we create a simple node, and then we create relationships.
Once, the relationships are created, i.e., we have a graph in memory, we calculate market fund values with different cases.

At the last, we print the result.


*******************************************************
*******************************************************

To run the unit test cases, please install nosetests (command: pip install nose), and run:

```nosetests -vds tests\test_graph.py```

```nosetests -vds tests\test_node.py```
